package Zadanie4;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Calculator calculator = new Calculator();
        do {

            System.out.println("Write a command: dodaj/odejmij/pomnoz/podziel [X] [Y]");
            String command = scanner.nextLine();
            if (command.equals("quit")) {
                System.out.println("Quitting");
                break;
            }
            try {
                String[] splitCommand = command.toLowerCase().trim().split(" ");
                String operation = splitCommand[0];
                String x = splitCommand[1];
                int x1 = Integer.parseInt(x);
                String y = splitCommand[2];
                int y1 = Integer.parseInt(y);

                switch (operation) {
                    case "dodaj":
                        System.out.printf("Result: %d \n", calculator.addTwoNumbers(x1, y1));
                        break;
                    case "odejmij":
                        System.out.printf("Result: %d \n", calculator.substractTwoNumbers(x1, y1));
                        break;
                    case "pomnoz":
                        System.out.printf("Result: %d \n", calculator.multiplyTwoNumbers(x1, y1));
                        break;
                    case "podziel":
                        System.out.printf("Result: %d \n", calculator.divideTwoNumbers(x1, y1));
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Niepoprawny format liczbowy");
            } catch (ArrayIndexOutOfBoundsException aiooobe) {
                System.out.println("Za malo parametrow");
            }
        } while (scanner.hasNextLine());
    }
}
