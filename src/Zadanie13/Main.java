package Zadanie13;

public class Main {
    public static void main(String[] args) {

        Student student = new Student(123123l, "Adam", "Kowalski");
        student.addScores(2);
        student.addScores(5);
        student.addScores(4);
        student.addScores(5);
        student.addScores(3);
        student.addScores(4);
        student.addScores(1);
        student.addScores(3);
        student.addScores(3);

        System.out.println(student.calculateMean());
        System.out.println(student.lowGradesInList());

    }
}
