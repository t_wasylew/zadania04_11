package Zadanie13;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private long indexNumber;
    private String name;
    private String surname;
    private List<Integer> scoresList = new ArrayList<>();

    public Student(long indexNumber, String name, String surname) {
        this.indexNumber = indexNumber;
        this.name = name;
        this.surname = surname;
    }

    public long getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(long indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Integer> getScoresList() {
        return scoresList;
    }

    public void setScoresList(List<Integer> scoresList) {
        this.scoresList = scoresList;
    }

    @Override
    public String toString() {
        return "Student{" +
                "indexNumber=" + indexNumber +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    public void addScores(int grade) {
        scoresList.add(grade);
    }

    public double calculateMean() {
        double sum = 0.0;
        for (double a : scoresList) {
            sum += a;
        }
        return sum / scoresList.size();
    }

    public boolean lowGradesInList() {
        System.out.println("Passed?");
        for (Integer grade : scoresList) {
            if (grade <= 2) {
                System.out.println("You shall not pass!");
                return false;
            }
        }
            System.out.println("Passed!");
            return true;
    }
}