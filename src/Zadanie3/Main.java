package Zadanie3;

public class Main {
    public static void main(String[] args) {
        Registry registry = new Registry();

        registry.addStudent("Tomek");
        registry.addStudent("Ada");
        registry.addStudent("Arek");
        registry.addStudent("Mietek");

        registry.printAllStudents();
        System.out.println();
        registry.removeStudent();
        registry.printAllStudents();

    }
}
