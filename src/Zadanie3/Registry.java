package Zadanie3;

import java.util.ArrayList;
import java.util.List;

public class Registry {

    private String[] studentlist = new String[30];
//    private List<String> studentList = new ArrayList<>();
    private int studentCount;

    public Registry() {
        this.studentCount = 0;
    }

    public void addStudent(String name) {
        studentlist[studentCount++]= name;
    }

    public void removeStudent(){
        studentCount--;
    }

    public void printAllStudents() {
        for (int i = 0; i < studentCount; i++) {
            System.out.println("Student no. " + i + " name: " + studentlist[i]);
        }
    }
}
