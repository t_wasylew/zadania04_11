package Zadanie7;

public class Main {
    public static void main(String[] args) {
        MyMath myMath = new MyMath();
        System.out.println(myMath.pow(2, 3));
        System.out.println(myMath.pow(2.0, 3));
        System.out.println(myMath.abs(-6));
        System.out.println(myMath.abs(5.0));
        System.out.println(myMath.abs(-5.0));
    }
}
