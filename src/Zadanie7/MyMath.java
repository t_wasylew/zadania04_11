package Zadanie7;

public class MyMath {

    public int abs(int a) {
        if(a >= 0){
            return a;
        }else {
            return a * (-1);
        }
    }

    public double abs(double a) {
        if(a >= 0){
            return a;
        }else {
            return a * (-1);
        }
    }

    public int pow(int a, int b) {
        int pow = a;
        if (b == 0) {
            return a;
        } else if (b < 0) {
            throw new NumberFormatException();
        } else {
            for (int i = 1; i < abs(b); i++) {
                a *= pow;
            }
            return a;
        }
    }

    public double pow(double a, int b) {
        double pow = a;
        if (b == 0) {
            return a;
        } else if (b < 0) {
            throw new NumberFormatException();
        } else {
            for (int i = 1; i < abs(b); i++) {
                a *= pow;
            }
            return a;
        }
    }
}
