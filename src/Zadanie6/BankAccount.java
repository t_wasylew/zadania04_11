package Zadanie6;

public class BankAccount {
    private double accountBalance;

    public BankAccount(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public double getAccountBalance() {
        return accountBalance;
    }


    public void addMoney(double income) {
        accountBalance += income;
    }

    public void substractMoney(double outcome) {
        accountBalance -= outcome;
    }

    public void printBankAccountStatus() {
        System.out.println("Your account balance is: " + getAccountBalance());
    }
}
