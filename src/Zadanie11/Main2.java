package Zadanie11;

import java.util.List;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main2 {


    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>(Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2));
        List<Integer> integerList2 = new ArrayList<>(Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2));
        int[] ints = new int[]{4, 2, 2, 1, 5, 29, 3, 8};

//        System.out.println(compare(integerList, ints));
//        List<Integer> list3 = copyList(integerList);
//        System.out.println(list3);

//        int[] arrayOfIntegers = chengeListToArray(integerList);

        System.out.println(isEqualList(integerList, integerList2));
}


    public static int compare(List<Integer> integerList, int[] ints) {
        int counter = 0;
        for (int i = 0; i < integerList.size(); i++) {
            if (integerList.get(i).equals(ints[i])) {
                counter++;
            }
        }
        return counter;
    }

    public static List<Integer> copyList(List<Integer> list) {
        return new ArrayList<>(list);
    }

    public static int[] changeListToArray(List<Integer> list) {
        int[] numbers = new int[list.size()];

        for (int i = 0; i < list.size(); i++) {
            numbers[i] = list.get(i);
        }
        return numbers;
    }

    public static List<Integer> changeArrayToList(int[] ints) {
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < ints.length; i++) {
            numbers.add(ints[i]);
        }
        return numbers;
    }

    public static List<Integer> mergingOfTwoLists(List<Integer> list, List<Integer> list2) {
        List<Integer> numbers = new ArrayList<>();
        numbers.addAll(list);
        numbers.addAll(list2);
        return numbers;
    }

    public static List<Integer> mergingOfLists(List<Integer>... lists) {
        List<Integer> integerList = new ArrayList<>();
        for (List<Integer> integer : lists) {
            integerList.addAll(integer);
        }
        return integerList;
    }

    public static <T> List<T> mergingOfLists2(List<T>... lists) {
        List<T> integerList = new ArrayList<>();
        for (List<T> integer : lists) {
            integerList.addAll(integer);
        }
        return integerList;
    }

    public static boolean isEqualList(List<Integer> list, List<Integer> list2) {
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(list2.get(i))) {
                count++;
            }
        }
        if (count == list.size()) {
            return true;
        }else {
            return false;
        }
    }
}


