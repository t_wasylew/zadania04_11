package Zadanie11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        List<Boolean> booleans = new ArrayList<>(Arrays.asList(true, true, true, true, false, false, false, false));

        System.out.println(countMean(integers));
        System.out.println(countSum(integers));

        System.out.println(coniungtionOfValues(booleans));
        System.out.println(alternativeOfValues(booleans));

        List<User> userList = new ArrayList<>(Arrays.asList(new User("Tomek", "123"), new User("Michal", "124"),
                new User("Ada", "125")));
        System.out.println(userList);

    }

    public static int countSum(List<Integer> list) {
        int sum = 0;
        for (int a : list) {
            sum += a;
        }
        return sum;
    }

    public static int countMultiplayer(List<Integer> list) {
        int multi = 1;
        for (int a : list) {
            multi *= a;
        }
        return multi;
    }

    public static int countMean(List<Integer> list) {
        return countSum(list) / list.size();
    }


    public static List<Boolean> negativeValue(List<Boolean> list) {
        boolean tmp;
        for (int i = 0; i <list.size() ; i++) {
            tmp = list.get(i);
            list.remove(i);
            list.add(i, !tmp);
        }
        return list;
    }
    public static List<Boolean> negativeValue2(List<Boolean> list) {
        boolean tmp;
        for (int i = 0; i <list.size() ; i++) {
            tmp = list.remove(i);
            list.add(i, !tmp);
        }
        return list;
    }

    public static List<Boolean> negativeValue3(List<Boolean> list) {
        for (int i = 0; i <list.size() ; i++) {
            list.set(i, !list.get(i));
        }
        return list;
    }

    public static boolean coniungtionOfValues(List<Boolean> list) {
        boolean value = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            value &= list.get(i);
        }
        return value;
    }
    public static boolean alternativeOfValues(List<Boolean> list) {
        boolean value = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            value |= list.get(i);
        }
        return value;
    }

}
